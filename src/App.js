import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Header from "./Components/Header/Header";
import PostsList from "./Containers/Posts/PostsList/PostsList";
import PreviewPost from "./Containers/Posts/PreviewPost/PreviewPost";
import AddPost from "./Containers/Posts/AddPost/AddPost";
import AboutUs from "./Components/AboutUs/AboutUs";
import Contacts from "./Components/Contacts/Contacts";

class App extends Component {

  render() {
    return (
      <div className="App">
        <Header/>
        <Switch>
          <Route path='/' exact component={PostsList}/>
          <Route path='/posts' exact component={PostsList}/>
          <Route path='/about' exact component={AboutUs}/>
          <Route path='/contacts' exact component={Contacts}/>
          <Route path='/posts/add' exact component={AddPost}/>
          <Route path='/posts/:id' exact component={PreviewPost}/>
          <Route path='/posts/:id/edit' exact component={AddPost}/>
        </Switch>
      </div>
    );
  }
}

export default App;
