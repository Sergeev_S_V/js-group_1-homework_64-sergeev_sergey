import React from 'react';
import './AboutUs.css';

const AboutUs = () => (
  <div className='AboutUs'>
    <p style={{fontWeight: 'bold'}}>About us</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad architecto aut in quasi quis tenetur veniam veritatis vitae, voluptates voluptatum.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto, at fugiat ipsum maxime possimus quod velit voluptatem. Consequuntur cumque dignissimos eos expedita facilis inventore minima molestiae nemo perspiciatis repudiandae.</p>
  </div>
);

export default AboutUs;