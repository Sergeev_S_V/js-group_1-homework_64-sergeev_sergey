import React from 'react';
import './Contacts.css';

const Contacts = () => (
  <div className='Contacts'>
    <p style={{fontWeight: 'bold'}}>Our contacts:</p>
    <p>number: +996 777 777-777</p>
    <ul>
      <li style={{fontWeight: 'bold'}}>Social: </li>
      <li><a href="#">Facebook</a></li>
      <li><a href="#">Instagram</a></li>
      <li><a href="#">Youtube</a></li>
      <li><a href="#">VK</a></li>
    </ul>
    <p>Address: USA, New york, street: my street</p>
  </div>
);

export default Contacts;