import React, {Component, Fragment} from 'react';
import axios from 'axios';
import Post from "../../../Components/Posts/Post/Post";
import Spinner from "../../../Components/UI/Spinner/Spinner";

class PostsList extends Component {

  state = {
    posts: {},
    loading: false,
  };

  getPostsFromAPI = () => {
    this.setState({loading: true}, () => {
      axios.get('/posts.json')
        .then(response => {
          if (response) {
            this.setState({posts: response.data, loading: false});
          }
        });
    })
  };

  readMoreHandler = (key) => {
    this.props.history.push(`/posts/${key}`);
  };

  componentDidMount() {
    this.getPostsFromAPI();
  };

  render() {
    let posts = (
      <Fragment>
        {this.state.posts
          ? Object.keys(this.state.posts).map(key => {
            return <Post key={key}
                         title={this.state.posts[key].title}
                         body={this.state.posts[key].body}
                         date={this.state.posts[key].date}
                         clicked={() => this.readMoreHandler(key)}/>
          })
          : <p style={{fontWeight: 'bold'}}>Add new post</p>}
      </Fragment>
    );

    this.state.loading ? posts = <Spinner/> : null;

    return(
      <div className="container">
        {posts}
      </div>
    );
  }
}

export default PostsList;