import React, {Fragment, Component} from 'react';
import axios from "axios/index";
import Button from "../../../Components/UI/Button/Button";

class PreviewPost extends Component {

  state = {
    title: '',
    body: '',
    date: '',
    loading: false,
  };

  editPostHandler = () => {
    this.props.history.push(`/posts/${this.props.match.params.id}/edit`);
  };

  deletePostHandler = () => {
    axios.delete(`/posts/${this.props.match.params.id}.json`)
      .then(() => this.props.history.push('/'));
  };

  componentDidMount() {
    this.setState({loading: true}, () => {
      this.props.match.params.id
        ? axios.get(`/posts/${this.props.match.params.id}.json`)
          .then(response => {
            this.setState({
              title: response.data.title,
              body: response.data.body,
              date: response.data.date,
              loading: false})
          })
        : this.setState({loading: false});
    });
  };


    render() {
      return(
        <Fragment>
          <p>Preview post</p>
          <p style={{fontWeight: 'bold'}}>Created on: {this.state.date}</p>
          <label>Title</label>
          <p className="form-control">{this.state.title}</p>
          <label>Description</label>
          <p className="form-control">{this.state.body}</p>
          <Button clicked={this.deletePostHandler}>Delete</Button>
          <Button clicked={this.editPostHandler}>Edit</Button>
        </Fragment>
      );
    }
}


export default PreviewPost;